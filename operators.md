    command1 | command2
-> `Executes command1 then executes command2 with command1's output`

    command1 > file_name
-> `Writes command1's output into file_name (no check, overwrites it)`

    command1 >> file_name
-> `Appends command1's output into file_name`